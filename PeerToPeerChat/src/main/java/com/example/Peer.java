package com.example;

import javax.json.Json;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.Socket;

public class Peer {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(">enter username & port for this peer");
        String[] setupValues = bufferedReader.readLine().split(" ");
        ServerThread serverThread = new ServerThread(setupValues[1]);
        serverThread.start();
        new Peer().updateListenToPeers(bufferedReader, setupValues[0], serverThread);
    }

    public void updateListenToPeers(BufferedReader bufferedReader, String username, ServerThread serverThread) throws Exception {
        System.out.println("> enter (space separated) !hello:port_number");
        System.out.println("> peers to receive message from (!skip to skip)");
        String input = bufferedReader.readLine();
        String[] inputValues = input.split(" ");
        if (!input.equals("!skip")) {
            for (String inputValue : inputValues) {
                String[] address = inputValue.split(":");
                Socket socket = null;
                try {
                    socket = new Socket("localhost", Integer.valueOf(address[1]));
                    PeerThread peerThread = new PeerThread(socket);
                    peerThread.start();
                } catch (Exception e) {
                    if (socket != null) {
                        socket.close();
                    } else {
                        System.out.println("Invalid input or unable to connect. Skipping to next step.");
                    }
                }
            }
        }
        communicate(bufferedReader, username, serverThread);
    }

    public void communicate(BufferedReader bufferedReader, String username, ServerThread serverThread) {
        try {
            System.out.println("You can now communicate (!bye_all to exit, !add to add another connection, !remove:port to remove a connection)");
            boolean flag = true;
            while(flag) {
                String message = bufferedReader.readLine();
                if("!bye_all".equals(message)) {
                    flag = false;
                    break;
                } else if("!add".equals(message)) {
                    updateListenToPeers(bufferedReader, username, serverThread);
                } else if(message.startsWith("!remove:")) {
                    int portToRemove = Integer.parseInt(message.substring(8));
                    serverThread.removeConnection(portToRemove);
                    System.out.println("Removed connection to port: " + portToRemove);
                } else {
                    StringWriter stringWriter = new StringWriter();
                    Json.createWriter(stringWriter).writeObject(Json.createObjectBuilder()
                            .add("username", username)
                            .add("message", message)
                            .build());
                    serverThread.sendMessage(stringWriter.toString());
                }
            }
            System.exit(0);
        } catch (Exception e) {
            System.out.println("Error during communication");
        }
    }
}