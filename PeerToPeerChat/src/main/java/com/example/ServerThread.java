package com.example;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashSet;
import java.util.Set;

public class ServerThread extends Thread{
    private ServerSocket serverSocket;
    private Set<ServerThreadThread> serverThreadThreads = new HashSet<ServerThreadThread>();

    public ServerThread(String portNumb) throws IOException {
        serverSocket = new ServerSocket(Integer.valueOf(portNumb));
    }

    public void run(){
        try {
            while (true) {
                ServerThreadThread serverThreadThread = new ServerThreadThread(serverSocket.accept(), this);
                serverThreadThreads.add(serverThreadThread);
                serverThreadThread.start();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    void sendMessage(String message) {
        try {
            serverThreadThreads.forEach(t -> t.getPrintWriter().println(message));
        }
        catch (Exception e) {
            System.out.println("Error sending message: " + e.getMessage());
        }
    }

    public Set<ServerThreadThread> getServerThreadThreads(){
        return serverThreadThreads;
    }

    public void removeConnection(int port) {
        serverThreadThreads.removeIf(thread -> {
            try {
                return thread.getSocket().getPort() == port;
            } catch (Exception e) {
                System.out.println("Could not remove given connection!");
                return false;
            }
        });
    }
}
