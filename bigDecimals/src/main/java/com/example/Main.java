package com.example;

import com.example.benchmarks.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class Main {
    public static void main(String[] args) throws Exception {
         org.openjdk.jmh.Main.main(args);
    }
}
