package com.example.benchmarks;

import com.example.operations.DoubleObjectOperations;
import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 2, time = 1)
@Measurement(iterations = 5, time = 1)
@Threads(8)
@Fork(1)
public class DoubleObjectRandomBenchmark {

    @State(Scope.Benchmark)
    public static class BenchmarkState {
        private List<Double> bigDoubleList = new ArrayList<>();;
        private Random random = new Random();
        public int size = 100000000;

        @Setup(Level.Iteration)
        public void setup() {
            bigDoubleList.clear();
            for (int i = 0; i < size; i++) {
                double randomDouble = 1000 * random.nextDouble();
                bigDoubleList.add(randomDouble);
            }
        }

        @Benchmark
        public void sumDoubleObject() {
            DoubleObjectOperations.sum(bigDoubleList);
        }

        @Benchmark
        public void averageDoubleObject() {
            DoubleObjectOperations.average(bigDoubleList);
        }

        @Benchmark
        public void topTenPercentDoubleObject() {
            DoubleObjectOperations.topTenPercent(bigDoubleList);
        }
    }
}
