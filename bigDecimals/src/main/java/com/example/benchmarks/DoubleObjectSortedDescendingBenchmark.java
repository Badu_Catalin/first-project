package com.example.benchmarks;

import com.example.operations.DoubleObjectOperations;
import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 10, time = 1)
@Fork(1)
public class DoubleObjectSortedDescendingBenchmark {

    @State(Scope.Benchmark)
    public static class BenchmarkState {
        private List<Double> bigDoubleList = new ArrayList<>();;
        public int size = 100000000;

        @Setup(Level.Iteration)
        public void setup() {
            bigDoubleList.clear();
            for (int i = size; i > 0; i--) {
                bigDoubleList.add((double) i);
            }
        }

        @Benchmark
        public void sumDoubleObject() {
            DoubleObjectOperations.sum(bigDoubleList);
        }

        @Benchmark
        public void averageDoubleObject() {
            DoubleObjectOperations.average(bigDoubleList);
        }

        @Benchmark
        public void topTenPercentDoubleObject() {
            DoubleObjectOperations.topTenPercent(bigDoubleList);
        }
    }
}

