package com.example.benchmarks;

import com.example.operations.DoublePrimitiveOperations;
import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 10, time = 1)
@Fork(1)
public class DoublePrimitiveSortedDescendingBenchmark {

    @State(Scope.Benchmark)
    public static class BenchmarkState {
        private double[] doubleArray;

        public int size = 100000000;

        @Setup(Level.Iteration)
        public void setup() {
            doubleArray = new double[size];
            for (int i = size; i > 0; i--) {
                doubleArray[i] = i;
            }
        }

        @Benchmark
        public void sumDoublePrimitive() {
            DoublePrimitiveOperations.sum(doubleArray);
        }

        @Benchmark
        public void averageDoublePrimitive() {
            DoublePrimitiveOperations.average(doubleArray);
        }

        @Benchmark
        public void topTenPercentDoublePrimitive() {
            DoublePrimitiveOperations.topTenPercent(doubleArray);
        }
    }
}
