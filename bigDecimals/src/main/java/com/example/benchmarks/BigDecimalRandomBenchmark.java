package com.example.benchmarks;

import com.example.operations.BigDecimalOperations;
import org.openjdk.jmh.annotations.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 2, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
public class BigDecimalRandomBenchmark {

    @State(Scope.Benchmark)
    public static class BenchmarkState {
        private List<BigDecimal> bigDecimalList = new ArrayList<>();;
        private Random random = new Random();
        public int size = 100000000;

        @Setup(Level.Iteration)
        public void setup() {
            bigDecimalList.clear();
            for (int i = 0; i < size; i++) {
                double randomDouble = 1000 * random.nextDouble();
                bigDecimalList.add(BigDecimal.valueOf(randomDouble));
            }
        }

        @Benchmark
        public void sumBigDecimal() {
            BigDecimalOperations.sum(bigDecimalList);
        }

        @Benchmark
        public void averageBigDecimal() {
            BigDecimalOperations.average(bigDecimalList);
        }

        @Benchmark
        public void topTenPercentBigDecimal() {
            BigDecimalOperations.topTenPercent(bigDecimalList);
        }
    }
}
