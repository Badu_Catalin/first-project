package com.example.benchmarks;

import com.example.operations.BigDecimalOperations;
import org.openjdk.jmh.annotations.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 2, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
public class BigDecimalSortedDescendingBenchmark {

    @State(Scope.Benchmark)
    public static class BenchmarkState {
        private List<BigDecimal> bigDecimalList = new ArrayList<>();;
        public int size = 100000000;

        @Setup(Level.Iteration)
        public void setup() {
            bigDecimalList.clear();
            for (int i = size; i > 0; i--) {
                bigDecimalList.add(BigDecimal.valueOf(i));
            }
        }

        @Benchmark
        public void sumBigDecimal() {
            BigDecimalOperations.sum(bigDecimalList);
        }

        @Benchmark
        public void averageBigDecimal() {
            BigDecimalOperations.average(bigDecimalList);
        }

        @Benchmark
        public void topTenPercentBigDecimal() {
            BigDecimalOperations.topTenPercent(bigDecimalList);
        }
    }
}

