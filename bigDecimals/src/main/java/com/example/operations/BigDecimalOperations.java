package com.example.operations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BigDecimalOperations {

    public static BigDecimal sum(List<BigDecimal> bigDecimals) {
        return bigDecimals.parallelStream()
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal average(List<BigDecimal> bigDecimals) {
        return bigDecimals.parallelStream()
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(new BigDecimal(bigDecimals.size()), 2, RoundingMode.HALF_UP);
    }

    public static List<BigDecimal> topTenPercent(List<BigDecimal> bigDecimals) {
        int topTenPercentCount = (int) Math.ceil(bigDecimals.size() * 0.1);
        return bigDecimals.parallelStream()
                .sorted(Comparator.reverseOrder())
                .limit(topTenPercentCount)
                .collect(Collectors.toList());
    }
}

