package com.example.operations;

import java.util.Arrays;

public class DoublePrimitiveOperations {

    public static double sum(double[] doubles) {
        return Arrays.stream(doubles).parallel().sum();
    }

    public static double average(double[] doubles) {
        return Arrays.stream(doubles).parallel().average().orElse(0.0);
    }

    public static double[] topTenPercent(double[] doubles) {
        int topTenPercentCount = (int) Math.ceil(doubles.length * 0.1);
        double[] sortedDoubles = Arrays.stream(doubles).parallel().sorted().toArray();
        return Arrays.copyOfRange(sortedDoubles, sortedDoubles.length - topTenPercentCount, sortedDoubles.length);
    }
}

