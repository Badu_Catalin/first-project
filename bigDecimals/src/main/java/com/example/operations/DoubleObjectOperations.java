package com.example.operations;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DoubleObjectOperations {

    public static Double sum(List<Double> doubles) {
        return doubles.parallelStream()
                .reduce(0.0, Double::sum);
    }

    public static Double average(List<Double> doubles) {
        return doubles.parallelStream()
                .mapToDouble(Double::doubleValue)
                .average()
                .orElse(0.0);
    }

    public static List<Double> topTenPercent(List<Double> doubles) {
        int topTenPercentCount = (int) Math.ceil(doubles.size() * 0.1);
        return doubles.parallelStream()
                .sorted(Comparator.reverseOrder())
                .limit(topTenPercentCount)
                .collect(Collectors.toList());
    }
}
