package com.example;

import com.example.operations.BigDecimalOperations;

import org.junit.jupiter.api.Test;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BigDecimalOperationsTest {

    @Test
    public void testSum() {
        List<BigDecimal> bigDecimals = Arrays.asList(BigDecimal.valueOf(10.5), BigDecimal.valueOf(20.5));
        BigDecimal expectedSum = BigDecimal.valueOf(31.0);
        assertEquals(expectedSum, BigDecimalOperations.sum(bigDecimals));
    }

    @Test
    public void testAverage() {
        List<BigDecimal> bigDecimals = Arrays.asList(BigDecimal.valueOf(10.5), BigDecimal.valueOf(20.0));
        BigDecimal expectedAvg = BigDecimal.valueOf(15.25);
        assertEquals(expectedAvg, BigDecimalOperations.average(bigDecimals));
    }

    @Test
    public void testTopTenPercent() {
        List<BigDecimal> bigDecimals = Arrays.asList(BigDecimal.valueOf(1), BigDecimal.valueOf(2), BigDecimal.valueOf(3), BigDecimal.valueOf(4), BigDecimal.valueOf(5), BigDecimal.valueOf(5), BigDecimal.valueOf(5), BigDecimal.valueOf(5), BigDecimal.valueOf(5), BigDecimal.valueOf(5));
        List<BigDecimal> expectedTopTen = Arrays.asList(BigDecimal.valueOf(5));
        assertEquals(expectedTopTen, BigDecimalOperations.topTenPercent(bigDecimals));
    }
}

