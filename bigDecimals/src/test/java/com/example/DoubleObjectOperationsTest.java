package com.example;

import com.example.operations.DoubleObjectOperations;

import org.junit.jupiter.api.Test;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DoubleObjectOperationsTest {

    @Test
    public void testSum() {
        List<Double> doubles = Arrays.asList(10.5, 20.6);
        Double expectedSum = 31.1;
        assertEquals(expectedSum, DoubleObjectOperations.sum(doubles));
    }

    @Test
    public void testAverage() {
        List<Double> doubles = Arrays.asList(10.0, 20.0, 30.0);
        Double expectedAvg = 20.0;
        assertEquals(expectedAvg, DoubleObjectOperations.average(doubles));
    }

    @Test
    public void testTopTenPercent() {
        List<Double> doubles = Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0);
        List<Double> expectedTopTen = Arrays.asList(10.0);
        assertEquals(expectedTopTen, DoubleObjectOperations.topTenPercent(doubles));
    }
}

