package com.example;

import com.example.operations.DoublePrimitiveOperations;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DoublePrimitiveOperationsTest {

    @Test
    public void testSum() {
        double[] doubles = {10.5, 20.6};
        double expectedSum = 31.1;
        assertEquals(expectedSum, DoublePrimitiveOperations.sum(doubles));
    }

    @Test
    public void testAverage() {
        double[] doubles = {10.0, 20.0, 30.0};
        double expectedAvg = 20.0;
        assertEquals(expectedAvg, DoublePrimitiveOperations.average(doubles));
    }

    @Test
    public void testTopTenPercent() {
        double[] doubles = {1.0, 2.0, 3.0, 4.0, 5.0, 4.0, 3.0, 2.0, 5.0, 1.0};
        double[] expectedTopTen = {5.0};
        assertArrayEquals(expectedTopTen, DoublePrimitiveOperations.topTenPercent(doubles));
    }
}
