package com.example.benchmarks;

import com.example.domain.Order;

import com.example.repositories.ConcurrentHashMapRepository;
import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 10, time = 1)
@Fork(1)
public class ConcurrentHashMapBenchmark {
    @State(Scope.Benchmark)
    public static class BenchmarkState {
        ConcurrentHashMapRepository<Order> repository = new ConcurrentHashMapRepository<>();
        Order order = new Order(1, 100, 1);
        public int size = 5;

        @Setup(Level.Iteration)
        public void setup() {
            repository.clear();
            for (int i = 0; i < size; i++) {
                repository.add(order);
            }
        }
    }

    @Benchmark
    public void add(BenchmarkState state)
    {
        for (int i = 0; i < state.size; i++)
        {
            state.repository.add(state.order);
        }
    }

    @Benchmark
    public void remove(BenchmarkState state) {
        for (int i = 0; i < state.size; i++)
        {
            state.repository.remove(state.order);
        }
    }

    @Benchmark
    public void contains(BenchmarkState state) {
        for (int i = 0; i < state.size; i++)
        {
            state.repository.contains(state.order);
        }
    }
}
