package com.example.repositories;

public interface InMemoryRepository<T> {
    void add(T item);
    void remove(T item);
    boolean contains(T item);
    void clear();
}
