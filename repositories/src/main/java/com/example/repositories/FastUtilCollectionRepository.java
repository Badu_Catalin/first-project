package com.example.repositories;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class FastUtilCollectionRepository<T> implements InMemoryRepository<T> {
    private final ObjectArrayList<T> arrayList = new ObjectArrayList<>();

    @Override
    public void add(T item) {
        arrayList.add(item);
    }

    @Override
    public void remove(T item) {
        arrayList.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return arrayList.contains(item);
    }

    @Override
    public void clear(){
        arrayList.clear();
    }
}
