package com.example.repositories;

import java.util.TreeSet;

public class TreeSetRepository<T extends Comparable<T>> implements InMemoryRepository<T> {
    private final TreeSet<T> set = new TreeSet<>();

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void clear(){
        set.clear();
    }
}

