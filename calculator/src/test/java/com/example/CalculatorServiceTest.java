package com.example;

import com.example.service.CalculatorService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CalculatorServiceTest {
    private CalculatorService calculatorService;

    @BeforeEach
    void setUp() {
        calculatorService = new CalculatorService();
    }

    @Test
    void testAddition() {
        double result = calculatorService.calculator("+", 5.0, 3.0);
        assertEquals(8.0, result);
    }

    @Test
    void testSubtraction() {
        double result = calculatorService.calculator("-", 5.0, 3.0);
        assertEquals(2.0, result);
    }

    @Test
    void testMultiplication() {
        double result = calculatorService.calculator("*", 5.0, 3.0);
        assertEquals(15.0, result);
    }

    @Test
    void testDivision() {
        double result = calculatorService.calculator("/", 6.0, 3.0);
        assertEquals(2.0, result);
    }

    @Test
    void testMinimum() {
        double result = calculatorService.calculator("min", 5.0, 3.0);
        assertEquals(3.0, result);
    }

    @Test
    void testMaximum() {
        double result = calculatorService.calculator("max", 5.0, 3.0);
        assertEquals(5.0, result);
    }

    @Test
    void testSquareRoot() {
        double result = calculatorService
                .calculator("sqrt", 16.0, -1.0); // The second number is ignored
        assertEquals(4.0, result);
    }

    @Test
    void testInvalidOperator() {
        assertThrows(IllegalArgumentException.class, ()
                -> calculatorService.calculator("unknown", 5.0, 3.0));
    }
}
