package com.example;

import com.example.controller.CalculatorController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CalculatorControllerTest {
    private CalculatorController calculatorController;

    private final ByteArrayOutputStream outputStream
            = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        calculatorController = new CalculatorController();
        System.setOut(new PrintStream(outputStream));
    }

    @Test
    public void testWorkingOperation() {
        provideInput("+\n5\n3\n");
        calculatorController.run();
        assertTrue(outputStream.toString().contains("Result: 8.0"));
    }

    @Test
    public void testInvalidOperation() {
        provideInput("unknown\n5\n3\n");
        calculatorController.run();
        assertTrue(outputStream.toString()
                .contains("Error: Invalid given operation!"));
    }

    private void provideInput(String input) {
        ByteArrayInputStream inputStream
                = new ByteArrayInputStream(input.getBytes());
        System.setIn(inputStream);
    }
}