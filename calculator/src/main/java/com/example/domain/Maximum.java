package com.example.domain;

public class Maximum implements IOperation {
    @Override
    public double calculator(double a, double b) {
        return Math.max(a, b);
    }
}