package com.example.domain;

public class SquareRoot implements IOperation {
    @Override
    public double calculator(double a, double b) {
        //We won't use b but keep the same method, so we can use "IOperation"
        return Math.sqrt(a);
    }
}
