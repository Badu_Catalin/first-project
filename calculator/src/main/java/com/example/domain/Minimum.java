package com.example.domain;

public class Minimum implements IOperation {
    @Override
    public double calculator(double a, double b) {
        return Math.min(a, b);
    }
}
