package com.example.controller;

import com.example.service.CalculatorService;
import java.util.Scanner;

public class CalculatorController {
    private final CalculatorService calculatorService;

    public CalculatorController(){
        calculatorService = new CalculatorService();
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter an operation (+, -, *, /, min, max, sqrt): ");
        String operator = scanner.next();

        System.out.print("Enter the first number: ");
        double a = scanner.nextDouble();

        if (!operator.equals("sqrt")) {
            System.out.print("Enter the second number: ");
        }
        double b = operator.equals("sqrt") ? -1 : scanner.nextDouble();

        try {
            double result = calculatorService.calculator(operator, a, b);
            System.out.println("Result: " + result);
        } catch (IllegalArgumentException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
