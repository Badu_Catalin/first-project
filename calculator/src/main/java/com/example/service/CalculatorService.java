package com.example.service;

import com.example.domain.Addition;
import com.example.domain.Division;
import com.example.domain.Subtraction;
import com.example.domain.Multiplication;
import com.example.domain.Minimum;
import com.example.domain.Maximum;
import com.example.domain.SquareRoot;
import com.example.domain.IOperation;
import java.util.Map;
import java.util.HashMap;

public class CalculatorService {
    private final Map<String, IOperation> operations;

    public CalculatorService(){
        operations = new HashMap<>();
        operations.put("+", new Addition());
        operations.put("-", new Subtraction());
        operations.put("*", new Multiplication());
        operations.put("/", new Division());
        operations.put("min", new Minimum());
        operations.put("max", new Maximum());
        operations.put("sqrt", new SquareRoot());
    }

    public double calculator(String operator, double a, double b){
        IOperation operation = operations.get(operator);
        if (operation == null){
            throw new IllegalArgumentException("Invalid given operation!");
        }
        return operation.calculator(a, b);
    }
}
